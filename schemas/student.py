from pydantic import BaseModel, Field
from schemas.classes import ClassData

from schemas.course import CourseData
from .generic import GenericModel
from uuid import UUID
from datetime import date
from typing import List


class StudentData(GenericModel):  
    cpf: str = Field(title="CPF of student", regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')  
    nome: str = Field(title='Name of student')
    data_aniversario: date | None = Field(default=None, title='Birth date of student')
    rg: str | None = Field(title='Number of identity card')
    orgao_expedidor: str | None = Field(title='Expediter of identity card')

    class Config:
        schema_extra = {
            "example": {
                "cpf":  "000.000.000-00",
                "nome": "Jane Smith",
                "data_aniversario": "1970-01-01",
                "rg": "0000000-0",
                "orgao_expedidor": "SSP"
                }
            }


class Student(StudentData):
    id: UUID = Field(title='ID of student')
    curso: CourseData | None = Field(title='Course of the student')
    disciplinas: list[ClassData] | None = Field(title='Classes of the student')

    class Config:
        schema_extra = {
            "example": {
                "id": "ecac7712-08e4-4bfb-a697-9018a74429fa",
                "curso": {
                    "id": "3eefcead-1be7-476c-a7b4-4778c3f738de",
                    "nome": "Engenharia Civil",
                    "ano_criacao": "1987",
                    "predio": "CTEC"
                },
                "disciplinas": [
                    {
                        "id": "f3a9f393-3d58-4082-a8b8-cd89a63465fe",
                        "nome": "Cálculo 1",
                        "descricao": "Limites, derivadas, integrais, etc",
                        "codigo": "MAT-01"
                    },
                    {
                        "id": "c97aa5a6-6284-4613-83b1-27d97d45e6e7",
                        "nome": "Mecânica dos Sólidos",
                        "descricao": "Elasticidade, tração, flexão, etc",
                        "codigo": "ECIV-51"
                    }
                ]
            }
        }

class StudentList(BaseModel):
    __root__: List[Student]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of students'
        }
