from pydantic import BaseModel, Field

from schemas.teachers import TeacherData
from .generic import GenericModel
from uuid import UUID
from typing import List


class CourseData(GenericModel):    
    nome: str = Field(title='Name of course')
    ano_criacao: str = Field(title='Year of course creation')
    predio: str = Field(title='Course building name')

    class Config:
        schema_extra = {
            "example": {
                "nome": "Engenharia Civil",
                "ano_criacao": "1987",
                "predio":  "CTEC"
                }
            }


class Course(CourseData):
    id: UUID = Field(title='ID of course')
    coordenador: TeacherData | None = Field(title='Course coordinator')

    class Config:
        schema_extra = {
            "example": {
                "id": "ecac7712-08e4-4bfb-a697-9018a74429fa",
                "coordenador": {
                    "cpf": "000.000.000-00",
                    "nome": "Barbirato",
                    "titulacao": "Doutorado"
                }
            }
        }

class CourseList(BaseModel):
    __root__: List[CourseData]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of courses'
        }
