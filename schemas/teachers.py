from pydantic import BaseModel, Field
from .generic import GenericModel
from uuid import UUID
from typing import List


class TeacherData(GenericModel):    
    cpf: str = Field(title="CPF of teacher", regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    nome: str = Field(title='Name of teacher')
    titulacao: str = Field(title='Teacher titration')

    class Config:
        schema_extra = {
            "example": {
                 "cpf": "000.000.000-00",
                "nome": "Roberto",
                "titulacao": "Doutorado"
                    }
                }


class Teacher(TeacherData):
    id: UUID = Field(title='ID of teacher')

    class Config:
        schema_extra = {
            "example": {
                "id": "ecac7712-08e4-4bfb-a697-9018a74429fa"
                }
            }

class TeacherList(BaseModel):
    __root__: List[Teacher]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of teachers'
        }