from pydantic import BaseModel, Field

from schemas.teachers import TeacherData
from .generic import GenericModel
from uuid import UUID
from typing import List


class ClassData(GenericModel):    
    nome: str = Field(title='Name of class')
    descricao: str = Field(title='Description of class')
    codigo: str = Field(title='Code of class')

    class Config:
        schema_extra = {
            "example": {
                "nome": "Cálculo 1",
                "descricao": "Limites, derivadas, integrais, etc",
                "codigo": "MAT-01"
                    }
                }


class Class(ClassData):
    id: UUID = Field(title='ID of class')
    professor: TeacherData | None = Field(title='Class teacher')

    class Config:
        schema_extra = {
            "example": {
                "id": "ecac7712-08e4-4bfb-a697-9018a74429fa",
                "professor": {
                    "cpf": "000.000.000-00",
                    "nome": "Roberto",
                    "titulacao": "Doutorado"
                }
            }
        }

class ClassList(BaseModel):
    __root__: List[ClassData]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of classes'
        }
