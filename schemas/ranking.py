from pydantic import BaseModel, Field
from .generic import GenericModel
from typing import List

class Ranking(GenericModel):    
    codigo: str = Field(title='Code of class')
    nome: str = Field(title='Name of class')
    quantidade: str = Field(title='Number of students in the class')

    class Config:
        schema_extra = {
            "example": {
                 "codigo": "MAT-01",
                "nome": "Cálculo 1",
                "quantidade": "0"
                    }
                }

class RankingList(BaseModel):
    __root__: List[Ranking]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'Ranking of classes'
        }