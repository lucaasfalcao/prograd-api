from msilib.schema import Class
from fastapi import FastAPI, HTTPException
import uvicorn
from uuid import UUID, uuid4
import sqlite3
from schemas.classes import ClassData, ClassList, Class
from schemas.course import Course, CourseData, CourseList
from schemas.ranking import RankingList
from schemas.student import StudentData, Student, StudentList
from schemas.teachers import Teacher, TeacherData, TeacherList

app = FastAPI(title="PROGRAD API")

connection = sqlite3.connect("./mydatabase.db")
client = connection.cursor()

# Students
student_tag = 'Students'

@app.get(
    '/students',
    summary='Fetch all students data',
    tags=[student_tag],
    response_model=StudentList
)
def get_students():
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    students = client.execute(f'SELECT * FROM alunos')
    return students.fetchall()

@app.get(
    '/students/{student_id}',
    summary='Fetch student data',
    tags=[student_tag],
    response_model=Student
)
def get_student(student_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    student = client.execute(f'SELECT * FROM alunos WHERE id = "{student_id}"')
    if student is None:
        raise HTTPException(status_code=404, detail=f'Student with id={student_id} not found')
    return student.fetchone()

@app.post(
    '/students',
    summary='Add a new student',
    tags=[student_tag],
    status_code=201,
    response_model=Student
)
def add_student(student_data: StudentData):
    student_id: UUID = uuid4()
    connection = sqlite3.connect("./mydatabase.db")
    client = connection.cursor()
    client.execute(
        f'INSERT INTO alunos values("{student_id}", "{student_data.cpf}", "{student_data.nome}", "{student_data.data_aniversario}", "{student_data.rg}", "{student_data.orgao_expedidor}", NULL)')
    connection.commit()
    return get_student(student_id=student_id)

@app.put(
    '/students/{student_id}/course/{course_id}',
    summary="Set or change student's course",
    tags=[student_tag],
    response_model=CourseData
)
def update_student_course(student_id: UUID, course_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    client.execute(
        f'UPDATE alunos SET curso_id = "{course_id}" WHERE id = "{student_id}"')
    connection.commit()
    return get_course(course_id=course_id)

@app.post(
    '/students/{student_id}/class/{class_id}',
    summary='Add student to a class',
    tags=[student_tag],
    status_code=201,
    response_model=ClassData
)
def add_student_into_class(student_id: UUID, class_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    client = connection.cursor()
    client.execute(
        f'INSERT INTO aluno_disciplina values("{class_id}", "{student_id}")')
    connection.commit()
    return get_class(class_id=class_id)

@app.delete(
    '/students/{student_id}/class/{class_id}',
    summary='Remove student from a class',
    tags=[student_tag],
    response_model=ClassData
)
def delete_student_into_class(student_id: UUID, class_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    client = connection.cursor()
    client.execute(
        f'DELETE FROM aluno_disciplina WHERE aluno_id = "{student_id}" AND disciplina_id = "{class_id}"')
    connection.commit()
    return get_class(class_id=class_id)


#Courses
course_tag = 'Courses'

@app.get(
    '/courses',
    summary='Fetch all courses data',
    tags=[course_tag],
    response_model=CourseList
)
def get_courses():
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    courses = client.execute(f'SELECT * FROM cursos')
    return courses.fetchall()

@app.get(
    '/courses/{course_id}',
    summary='Fetch course data',
    tags=[course_tag],
    response_model=CourseData
)
def get_course(course_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    course = client.execute(f'SELECT * FROM cursos WHERE id = "{course_id}"')
    if course is None:
        raise HTTPException(status_code=404, detail=f'Course with id={course_id} not found')
    return course.fetchone()

@app.post(
    '/courses',
    summary='Add a new course',
    tags=[course_tag],
    status_code=201,
    response_model=Course
)
def add_course(course_data: CourseData):
    course_id: UUID = uuid4()
    connection = sqlite3.connect("./mydatabase.db")
    client = connection.cursor()
    client.execute(
        f'INSERT INTO cursos values("{course_id}", "{course_data.nome}", "{course_data.ano_criacao}", "{course_data.predio}", NULL)')
    connection.commit()
    return get_course(course_id=course_id)

@app.put(
    '/courses/{course_id}/coordinator/{teacher_id}',
    summary="Set or change a professor as the course coordinator",
    tags=[course_tag],
    response_model=Teacher
)
def update_course_coordinator(course_id: UUID, teacher_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    client.execute(
        f'UPDATE cursos SET coordenador = "{teacher_id}" WHERE id = "{course_id}"')
    connection.commit()
    return get_teacher(teacher_id=teacher_id)

@app.get(
    '/courses/{course_id}/students',
    summary='Fetch students of course',
    tags=[course_tag],
    response_model=StudentList
)
def get_students_of_course(course_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    students = client.execute(f'SELECT * FROM alunos WHERE alunos.curso_id = "{course_id}"')
    if students is None:
        raise HTTPException(status_code=404, detail=f'Students of course with id={course_id} not found')
    return students.fetchall()


#Teachers
teacher_tag = 'Teachers'

@app.get(
    '/teachers',
    summary='Fetch all teachers data',
    tags=[teacher_tag],
    response_model=TeacherList
)
def get_teachers():
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    teachers = client.execute(f'SELECT * FROM professores')
    return teachers.fetchall()

@app.get(
    '/teachers/{teacher_id}',
    summary='Fetch teacher data',
    tags=[teacher_tag],
    response_model=Teacher
)
def get_teacher(teacher_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    teacher = client.execute(f'SELECT * FROM professores WHERE id = "{teacher_id}"')
    if teacher is None:
        raise HTTPException(status_code=404, detail=f'Teacher with id={teacher_id} not found')
    return teacher.fetchone()

@app.post(
    '/teachers',
    summary='Add a new teacher',
    tags=[teacher_tag],
    status_code=201,
    response_model=Teacher
)
def add_teacher(teacher_data: TeacherData):
    teacher_id: UUID = uuid4()
    connection = sqlite3.connect("./mydatabase.db")
    client = connection.cursor()
    client.execute(
        f'INSERT INTO professores values("{teacher_id}", "{teacher_data.cpf}", "{teacher_data.nome}", "{teacher_data.titulacao}")')
    connection.commit()
    return get_teacher(teacher_id=teacher_id)

#Classes
class_tag = 'Classes'

@app.get(
    '/classes',
    summary='Fetch all classes data',
    tags=[class_tag],
    response_model=ClassList
)
def get_classes():
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    classes = client.execute(f'SELECT * FROM disciplinas')
    return classes.fetchall()

@app.get(
    '/classes/{class_id}',
    summary='Fetch class data',
    tags=[class_tag],
    response_model=ClassData
)
def get_class(class_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    classe = client.execute(f'SELECT * FROM disciplinas WHERE id = "{class_id}"')
    if classe is None:
        raise HTTPException(status_code=404, detail=f'Class with id={class_id} not found')
    return classe.fetchone()

@app.post(
    '/classes',
    summary='Add a new class',
    tags=[class_tag],
    status_code=201,
    response_model=Class
)
def add_class(class_data: ClassData):
    class_id: UUID = uuid4()
    connection = sqlite3.connect("./mydatabase.db")
    client = connection.cursor()
    client.execute(
        f'INSERT INTO disciplinas values("{class_id}", "{class_data.nome}", NULL, "{class_data.descricao}", "{class_data.codigo}")')
    connection.commit()
    return get_class(class_id=class_id)

@app.put(
    '/classes/{class_id}/teacher/{teacher_id}',
    summary="Set or change a professor as the class teacher",
    tags=[class_tag],
    response_model=Teacher
)
def update_class_teacher(class_id: UUID, teacher_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    client.execute(
        f'UPDATE disciplinas SET professor = "{teacher_id}" WHERE id = "{class_id}"')
    connection.commit()
    return get_teacher(teacher_id=teacher_id)

@app.get(
    '/classes/{class_id}/students',
    summary='Fetch students of class',
    tags=[class_tag],
    response_model=StudentList
)
def get_students_of_class(class_id: UUID):
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    students = client.execute(f'SELECT alunos.* FROM alunos JOIN aluno_disciplina ON alunos.id = aluno_disciplina.aluno_id and aluno_disciplina.disciplina_id = "{class_id}"')
    if students is None:
        raise HTTPException(status_code=404, detail=f'Students of class with id={class_id} not found')
    return students.fetchall()

@app.get(
    '/disciplines/ranking',
    summary='Get a ranking of classes by the number of their enrolled students',
    tags=[class_tag],
    response_model=RankingList
)
def get_ranking_of_classes():
    connection = sqlite3.connect("./mydatabase.db")
    connection.row_factory = sqlite3.Row
    client = connection.cursor()
    ranking = client.execute(f'SELECT disciplinas.codigo, disciplinas.nome, COUNT(*) as "quantidade" FROM disciplinas INNER JOIN aluno_disciplina ON aluno_disciplina.disciplina_id = disciplinas.id GROUP BY disciplinas.nome ORDER BY COUNT(*) DESC')
    return ranking.fetchall()


if __name__ == '__main__':
    uvicorn.run(app='main:app', port=3000, reload=True)





